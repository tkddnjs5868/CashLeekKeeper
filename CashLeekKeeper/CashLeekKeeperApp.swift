//
//  CashLeekKeeperApp.swift
//  CashLeekKeeper
//
//  Created by SangWon Choi on 2022/10/03.
//

import SwiftUI

@main
struct CashLeekKeeperApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
